import express from 'express';
import httpProxy from 'http-proxy';

var server = express();

var apiForwardingUrl = 'https://my-domain.com/';

server.set('port', 3000);
server.use(express.static(__dirname + '/static/'));

var apiProxy = httpProxy.createProxyServer();

server.all("/api/*", function (req, res) {
  apiProxy.web(req, res, {
    target: apiForwardingUrl, changeOrigin: true,
  });
});

server.get('/*', (req, res) => {
  res.sendFile(__dirname + '/static/index.html');
})

server.listen(server.get('port'), function () {
  console.log('Express server listening on port ' + server.get('port'));
});
